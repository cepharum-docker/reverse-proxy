#!/bin/sh

echo "generating HA Proxy configuration:"

export BIND_PORT="${BIND_PORT:-80}"
export PEER_MODE="${PEER_MODE:-http}"
export PEER_PORT="${PEER_PORT:-80}"

[ -z "${PEER_ADDRESS:-}" ] && { error "missing peer's hostname or IP address in PEER_ADDRESS" >&2; exit 1; }

envsubst </opt/haproxy.cfg | tee /usr/local/etc/haproxy/haproxy.cfg

exec /bin/sh /docker-entrypoint.sh "$@"
