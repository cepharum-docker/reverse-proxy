# cepharum/reverse-proxy

most simple reverse proxy for use in stacks

## Source Code & Issues

There is a [public git repository](https://gitlab.com/cepharum-docker/reverse-proxy) for inspecting this image in detail.

## Usage

This image is applying minimal extension to [official image of HA-Proxy](https://hub.docker.com/_/haproxy/) to have a simple reverse proxy in HTTP mode with peer information controlled via environment variables. It is meant to be used in conjunction with other services for exposing their web interface in situations like this one:

* There is a docker swarm.
* An edge router like [Traefik](https://docs.traefik.io/) is used to expose some services of separate stacks to the public. All those services have to join network **cloud-edge**.

 > Examples given below omit configuration regarding integration with an edge router for sake of illustrating the essential problem.

* You are going to have several database engines provided for shared use by stacks of your swarm, like [ReThinkDB](https://rethinkdb.com/). 
  * You want to control access on either database engine by having all consuming services sharing distinct network with either database engine.
  * For every provided database engine in its distinct internal network, you want it to be accessible via some simple hostname such as **db**. 
* This database engine comes included with a web-based administration GUI you want to access remotely, thus need to expose it to the public using edge router.

You might end up with a solution like this one:

```yaml
version: "3.7"

services:
  db:
    image: rethinkdb
    networks:
      - db-rethink
      - cloud-edge

networks:
  cloud-edge:
    external: true
```

In this case a host named **db** is joining network **cloud-edge** used for exposing all services of swarm to the public. Thus, either service exposed that way is capable of reaching this particular database engine using hostname **db**. And you can't have different database engines exposed to different services joining different networks as hostname **db**, anymore.

You may fix this issue by using some prefix to imitate the namespace support currently missing in overlay networks of Docker swarm.

```yaml
version: "3.7"

services:
  kdiehsncnxk-db:
    image: rethinkdb
    networks:
      - db-rethink
      - cloud-edge

networks:
  cloud-edge:
    external: true
```

By prepending a random string to the service name, it is possible to prevent hostname clashes in services bound to **cloud-edge** network. However, this prefix must be used with all services consuming this database engine. In addition, separating database engines is sort of weakened.

Now, this is when **cepharum/reverse-proxy** is getting useful:

```yaml
version: "3.7"

services:
  db:
    image: rethinkdb
    networks:
      - db-rethink

  xdgehfirjdls-my-custom-proxy:
    image: cepharum/reverse-proxy
    networks:
      - db-rethink
      - cloud-edge
    environment:
      PEER_ADDRESS: db
      PEER_PORT: 8080

networks:
  db-rethink:
    external: true
  cloud-edge:
    external: true
```

This time, the database engine is keeping its simple hostname. Except for its web-based GUI, any service interested in accessing this database engine must join the engine's network explicitly.
