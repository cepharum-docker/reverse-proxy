FROM haproxy:alpine

COPY files/ /opt/
RUN apk add --no-cache gettext && chmod +x /opt/start.sh

ENTRYPOINT ["/bin/sh", "/opt/start.sh"]
CMD ["haproxy", "-f", "/usr/local/etc/haproxy/haproxy.cfg"]
